//
//  calendarApp.swift
//  calendar
//
//  Created by agile02 on 2021/10/29.
//

import SwiftUI

@main
struct calendarApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
